# EDL 2022

[![pipeline status](https://gitlab.com/e-henry/pls2022-students/badges/main/pipeline.svg)](https://gitlab.com/e-henry/pls2022-students/-/tree/main)

Principe : remplir un fichier au format [YAML](https://yaml.org/) pour permettre
la mise en place de groupes de développement github et gitlab ainsi qu'une infra
qui servira à la réalisation de certains TD.

## Installation

Procédure standard d'un projet nodejs : 

```shellsession
$ npm ci
```

## Test en local

```shellsession
$ npm run test
```

